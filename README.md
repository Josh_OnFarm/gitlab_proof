# Gitlab Proof

[Verifying my OpenPGP key: openpgp4fpr:585999f6c6b3efd61af36c69b20367adcf0cfcb4]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs
